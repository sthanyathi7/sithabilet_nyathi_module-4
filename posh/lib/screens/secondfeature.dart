import 'package:flutter/material.dart';

class PromotionsPage extends StatelessWidget {
  const PromotionsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              title: const Text("Promotions PAGE"),
            ),
            body: Padding(
              padding: const EdgeInsets.all(38.0),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const <Widget>[
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          "PROMOTIONS FOR THE MONTH!!",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Text("Hair and Beard Trim;"),
                    Text("Full set of nails and eyebrow wax;"),
                    Text("Haircut and Treatment;"),
                    Text("Pixie Cut and Color Highlight"),
                    Image(
                      image: AssetImage('images/saloon04.png'),
                      height: 200,
                    ),
                    Text(
                      "Learn more about our promotions instore",
                      style: TextStyle(
                          fontWeight: FontWeight.w900,
                          color: Colors.blueAccent),
                    ),
                  ],
                ),
              ),
            )));
  }
}
