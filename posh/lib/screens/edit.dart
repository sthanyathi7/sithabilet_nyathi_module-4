import 'package:flutter/material.dart';
import './screen_index.dart';

class Edit extends StatelessWidget {
  const Edit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text("USER EDIT PROFILE"),
            ),
            body: Padding(
              padding: const EdgeInsets.all(28.0),
              child: Container(
                padding: const EdgeInsets.fromLTRB(5, 25, 50, 10),
                child: Column(
                  children: [
                    const Center(
                      child: Text('EDIT YOUR INFORMATION HERE!',
                          style: TextStyle(fontSize: 18, height: 2)),
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: 'Username',
                      ),
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: 'Email',
                      ),
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: 'First Name',
                      ),
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: 'Last Name',
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.fromLTRB(5, 25, 50, 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          TextButton(
                            style: ElevatedButton.styleFrom(
                              // Foreground color
                              onPrimary: Theme.of(context)
                                  .colorScheme
                                  .onSecondaryContainer,
                              // Background color
                              primary: Theme.of(context)
                                  .colorScheme
                                  .secondaryContainer,
                            ).copyWith(
                                elevation: ButtonStyleButton.allOrNull(0.0)),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Registration()),
                              );
                            },
                            child: const Text(
                              'Reset',
                            ),
                          ),
                          TextButton(
                            style: ElevatedButton.styleFrom(
                              // Foreground color
                              onPrimary: Theme.of(context)
                                  .colorScheme
                                  .onSecondaryContainer,
                              // Background color
                              primary: Theme.of(context)
                                  .colorScheme
                                  .secondaryContainer,
                            ).copyWith(
                                elevation: ButtonStyleButton.allOrNull(0.0)),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Dashboard()),
                              );
                            },
                            child: const Text(
                              'Save',
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )));
  }
}
